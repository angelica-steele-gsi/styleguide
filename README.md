# Styleguide - GSIHealth Coordinator

* Note: Hexo is awesome! Save your changes and the browser auto-refreshes in the same spot and immediately reflects your changes. No need to rebuild, redeploy, or recompile.

* To the next developer/designer/unicorn taking this over: 

> I'm developing this project in my last 1 1/2 weeks here. Naturally, it's not finished. There are artifacts left over from the repo I forked this from, some broken links, and things that need to be filled in. With the time constraint in mind, my goal writing this is mainly to serve as a framework that GSI can fill in with more of its own content later on. I basically fleshed out the "table of contents" I've envisioned for a styleguide; but only have time to give links to resources that can be used to fill in these pages, not fill them in myself. 

> I worked as both the "UI/UX Guru" as well as a developer here, so all of the links/resources I'm providing are the standards & guides I used to try to push GSI toward a more modern UI. 

> A strong weakness of Coordinator is that it does not have a solid direction or set of standards regarding design. Behind a lot of style and structure choices there is a lack of clear intent. Coordinator runs on 4+ UI frameworks, so there is also little consistency. Hopping between pages feels like jumping to an entirely different application. In addition, our developers are not unified in their knowledge of our system and how it runs or operates. Many times knowledge is hoarded and then lost upon employee departure. This guide is meant to set standards for GSI to follow to overcome this weakness and provide users with a seamless, comfortable, intentional experience. 

----------

## How to Build

* Run all build cmds in root of this project. Make sure you run your cmd prompt "As Administrator" to avoid permission issues.

> npm install hexo-cli -g

> * This installs Hexo, the framework running this project, to your local machine. This allows you to run "hexo" cmds

> npm install
> hexo generate
> hexo serve

> * This starts the server to view the UI; it defaults to port 4000. If that is in use, run;

> hexo serve -p 5000

> * Replace the "5000" with an empty port to run on
> * To access project, go to http://localhost:4000
> * If you replaced 4000 with a separate port, replace the 4000 in the URL with that port #

> * As long as the server is running, you should be able to view the UI
> * If you can't see the UI, check if the server is up
> * If it is, and it's still not showing, it's most likely a bug in your code

> * To remove generated local files created with "hexo generate", run

> hexo clean

> * Do this before running "hexo generate" again
> * This is not really needed, but could be used for troubleshooting

> * Once Quasar Play is implemented, check the "build" file in root for cmds to build

----------

## Resources

* Hexo documentation (what this projects is written in)
> https://hexo.io/

* Quasar documentation (for when Quasar Play is implemented)
> https://quasar-framework.org/

* Font Awesome documentation (icons being used in this project - NOT Coordinator)
> https://fontawesome.com/

* Quasar Framework Website Github (what this project was forked from)
> https://github.com/quasarframework/quasar-framework.org

* Markdown Cheatsheet (language most pages are written in; easy to learn even if you've never used it)
> https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

----------

## Future Plans

* Implementing Quasar Play

> Quasar Play is a platform for viewing/messing with code written in Quasar
> It can be used to:
> * Take shortcode components for a test-run
> * Push a Quasar project to the "Quasar Play" app on mobile devices to preview
> * And more!

> Preview Quasar Play here:
> * https://quasar-framework.org/components/field.html
> * Quasar Play is running in the emulated phone on the right-hand side
