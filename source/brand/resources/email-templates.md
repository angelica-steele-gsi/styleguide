title: eMail Templates
---
The following are templates for use in eMails with clients, partners, prospects, etc.

## eMail Signature

**LeRoy Jones** 
President & Chief Executive Officer 
**P** 888.206.4237 ext. 222 | **F** 888.423.8759

![GSIHealth - Logo](/images/logo/110x25.jpg)
 
**GSI Health** | 1735 Market Street | 53rd Floor | Philadelphia, PA 19103
leroy.jones@gsihealth.com | www.gsihealth.com

CONNECT WITH US
[Facebook](https://www.facebook.com/GSIHealth/) | [Twitter](https://twitter.com/GSIHealth) | [LinkedIn](https://www.linkedin.com/company/296090) | [Google+](https://plus.google.com/117397150400031912311/posts)

