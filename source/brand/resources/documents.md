title: Documents
---
The following are templates and example documents for reference when creating GSIHealth branded documentation.

## PowerPoint Presentations

[PowerPoint Presentation Styleguide](/documents/examples/Presentation-Style-Guide.pptx)

## White Papers

[White Paper Example #1](/documents/examples/Example-White-Paper.pdf)
[White Paper Example #2](/documents/examples/Example-White-Paper-2.pdf)
[White Paper Example #3](/documents/examples/Example-White-Paper-3.pdf)
[White Paper Example #4](/documents/examples/Example-White-Paper-4.pdf)

## Success Stories

[Success Story Example](/documents/examples/Example-Success-Story.pdf)

## Banners

[Banner Example](/documents/examples/Example-Banner.pdf)