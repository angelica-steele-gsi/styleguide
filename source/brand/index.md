title: Get to know your company
---

![GSIHealth - Team](/images/team.JPG)

## Who is GSIHealth?
GSIHealth is a company that develops "Software as a Service" ([SaaS](https://en.wikipedia.org/wiki/Software_as_a_service)) for the medical industry.

GSI is based in Philadelphia, with a second office in NYC. Our goal is to offer our services internationally, but as of now, we are available in the following states:

* [New York](https://www.gsihealth.com/solving-new-yorks-population-health-management-challenges/)
* [Pennsylvannia](https://www.gsihealth.com/the-digital-health-initiative-of-philadelphia/)
* [Washington](https://www.gsihealth.com/solving-washingtons-population-health-management-challenges/)
* [Oregon](https://www.gsihealth.com/solving-oregons-population-health-management-challenges/)
* [California](https://www.gsihealth.com/solving-californias-population-health-management-challenges/)

Our flagship product is GSIHealth Coordinator, though we have other tools made to work alongside it, and internal tools we have developed to improve our own development process. The following is a list of the products we offer to our clients.

## Coordinator
Coordinator is a [population health platform](https://www.gsihealth.com/population-health-management/). In layman's terms, it's web application used by hospitals and other care providers to [coordinate patients' care](https://www.gsihealth.com/population-health-management/care-coordination-and-care-management/). Doctors, nurses, and other healthcare specialists can access and share patient information through our platform to provide quicker, more specialized, more up-to-date care. 

[Learn more about Coordinator]().

## Patient Loader
Patient Loader is a tool for loading new patients into the Coordinator platform.

[Learn more about Patient Loader]().
