title: Imagery
---
Generally, we use photography over illustrations.

We will likely create some presentation imagery and/or infographics that are illustrations this year—but look and feel is TBD.

Photography shown on our [website](https://gsihealth.com/) is a good example of the type of imagery we use; happy faces, nice colors, sun-drenched, etc.

**Avoid** using images showing pen/paper - this is not appropriate for a company that provides a software platform.
