title: Logos
---
The following is a library of all of our logos, at all sizes, from large, to small. The first of each duplicate is a PNG (transparent background), the second is a JPG (solid white background).

## Default - Logo With Text

**110 x 25**

![GSIHealth - Logo](/images/logo/110x25.png)
![GSIHealth - Logo](/images/logo/110x25.jpg)

**250 x 52**

![GSIHealth - Logo](/images/logo/250x52.png)
![GSIHealth - Logo](/images/logo/250x52.jpg)

**1800 x 380**

![GSIHealth - Logo](/images/logo/1800x380.png)
![GSIHealth - Logo](/images/logo/1800x380.jpg)

**6828 x 1340**

![GSIHealth - Logo](/images/logo/6828x1340.png)
![GSIHealth - Logo](/images/logo/6828x1340.jpg)

## Logo With Tagline

![GSIHealth - Logo](/images/logo/tagline.png)

## Standalone Logo

![GSIHealth - Logo](/images/logo/xl.png)