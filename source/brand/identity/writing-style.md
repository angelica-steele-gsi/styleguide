title: Writing Style
---

**GSIHealth’s voice is:**
* Conversational and engaging
* Crisp
* Client-centric
* Assertive and confident in tone

**When writing...**
* Use language that is easy to read and understand 
    * Short, simple sentences that speak directly to our audience
    * Remember that our users come from different educational and language backgrounds, and many may not be tech-savvy
* Use active voice, avoid passive voice
* Use first person over third person (“you” vs. “the user”)
* Use crisp, concise writing—prune non-impactful verbiage
* Emphasize benefits vs. being GSI Health-centric (e.g., talk about how our solutions can solve problems vs. what we offer)
* Avoid hype (e.g., exclamation points, excessive superlatives)

## Punctuation
* Don’t use end punctuation in bullets except when there is more than one sentence in the bullet
* Use em dashes (long dashes, not hyphens) instead of semicolons to break up long sentences—but use simple sentences when possible
* Use a comma before “and” and “or” in a series (coffee, tea, and water)
* When hyphenating a phrase in a title/header, or if it’s part of a proper noun, capitalize the first letter after the hyphen as well (World-Class Support)
* Avoid ampersands—spell out “and” instead
    * Exceptions include terms such as R&D, Q&A

## Dates
* Do not add letters after the date (July 26, not July 26th)
* Spell out (July 26, 2018) or use U.S. format of MM/DD/YY rather than European format of DD/MM/YY

## Numbers
* Put zeros in front of decimals (0.9%)
* Spell out numbers one through nine, and use numerals for 10, 11, 12…
    * Exceptions may be made for data descriptions, lists, paragraphs that have many numbers, etc. 
* To show a range, use either the word “to” or an en dash (not a hyphen) surrounded by spaces, whichever looks best in context (60 to 90 days, 7:00am – 7:00pm)
* Apply commas in numbers over 3 digits (2,000, not 2000)

## Miscellaneous

* Acronyms:
    * Spell out first instance, with acronym in parentheses: population health management (PHM)
    * Do not initial cap the spell-out unless it is a proper name
* Usage of e.g. and i.e.:
    * E.g. means “for example”
    * I.e. means “that is to say” or “in other words”
    * Use lowercase, a period after each letter, and followed by a comma, i.e., like this 
* Use **bold** and *italic* for emphasis, never underline.
* Writing style reference: Chicago Manual of Style

