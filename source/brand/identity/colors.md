title: Colors
---
The following is a spec on GSIHealth's brand colors. This is separate from our application color palette. [See that here](/style/our-standards/color-palette.html).

![GSIHealth Color Specs](/images/color-specs.png)