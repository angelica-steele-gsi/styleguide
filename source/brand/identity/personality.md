title: Personality
---
Just like a person, our company has a voice. Our voice is how we connect to our customers, employees, investors, and partners. What we say — and how we say it — shapes peoples’ understanding of who we are and contributes to how they feel about our company and technology. If we all use the same voice consistently, then everything we write can help strengthen our brand. 

* **MISSION-DRIVEN** — Seeing Healthcare Solved™ is our passion—the heartbeat of why we exist. We strive to make a difference for the health of patients and populations across the nation. 
* **CLIENT-CENTRIC** — We are true partners with our clients throughout the lifetime of each engagement. We continually listen and collaborate closely to make their care delivery programs more effective. Customer satisfaction is the barometer of our success. 
* **PROGRESSIVE** — We lead rather than follow. We think to the future, driving innovation and evolving our solution to address the ever-changing healthcare landscape and to tackle not just today’s challenges, but tomorrow’s. Further, we drive our industry to innovate, pushing to ensure that the ecosystem of healthcare evolves together, and succeeds collaboratively.
* **INSPIRATIONAL** — The success of our clients comes first, because they are doing the hard work of improving the lives of those that need it. Anything we can do to enable and strengthen that work counts more to us than our corporate prosperity. Truly healthy populations are our primary payment.
* **PROBLEM SOLVER** — We thrive on empowering our clients and solving their challenges. We combine deep industry expertise with fresh ideas, and turn them into dependable solutions that improve outcomes and ROI. 
