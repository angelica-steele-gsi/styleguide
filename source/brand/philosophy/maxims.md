title: Maxims
---
Maxims are the guidelines we set for ourselves as a group of people working to achieve the same goal. To keep ourselves grounded and focused on our mission, we adhere to these ideas and do our best to make them shine through everything we do.

<object data="/documents/Coorporate-Maxims.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="/documents/Coorporate-Maxims.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="/documents/Coorporate-Maxims.pdf">Download PDF</a>.</p>
    </embed>
</object>