title: Admin App
---
The Administrator App is one of 3 that is not available to all users. Only users with administrative privileges within their organization will see it appear when they log in to Coordinator. Through the Admin App, you can add and update both users & organizations.