title: Demographics App
---
The Demographics App is pretty self-explanitory. Users can add patients here. They can also modify them, or add them to their [patient panel](components/patient-panel.html).