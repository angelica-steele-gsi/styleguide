title: Care Teams App
---
The Care Teams App displays a searchable list of users that when selected, displays their information and gives users the option to view, modify and create a Care Team. Care Teams can also be created on the main page of the Care Teams App.