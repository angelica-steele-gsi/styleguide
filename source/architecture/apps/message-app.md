title: Message App
---
The Message App is also pretty straightforward; users can send and recieve messages similar to emails. They can sort their messages into folders, mark them for follow-up, set up auto-replies, assign delegates, and access a list of contacts here.