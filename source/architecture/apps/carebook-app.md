title: CareBook App
---
CareBook displays multiple tables of information on patients, including their demographic & enrollment information, their Care Team, and summaries on several other elements, such as tasks and alerts regarding them, test results, allergies they have and medications they take. Care Book also has built in search functionality in the header throughout all of Coordinator