title: Alerts App
---
Users receive Alerts from multiple parts of the application, which appear in this app. Users will be alerted on updates on patient enrollment, demographic information, added goals, patient encounters, and more.