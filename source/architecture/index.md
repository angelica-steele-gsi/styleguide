title: Application Architecture
---

![GSIHealth - Login](/images/architecture/login.png)

## What is this architecture for?
Good question. Not all of our architecture is focused on our main product (or its current version). Currently, we have documentation on 3 categories:

### GSIHealth Coordinator - Redux+
This encompases the more modern, recently updated and/or created side our flagship product. Most of the documentation here is based around it.

"Redux" is the system written to make Coordinator compatible with Angular, NodeJS, and generally move toward a more modern stack. Redux was implemented in 2016, and caused a **massive** makeover for the product.

"Actor" is an even newer system written in 2018 to catapult Coordinator even further, some features resting on the cutting-edge of technology. Actor implemented Vue, rapid templating, internationalization, and better configuration.

**Coordinator Post-2016:**

![GSIHealth - Coordinator Post-2016](/images/architecture/redux+.png)

### GSIHealth Coordinator - Legacy
We also have documentation on the old version of Coordinator, before we implemented a lot of new technologies and gutted out outdated ones. There are still traces of this version left over in Redux+, so we keep this documentation here instead of an archive so we can reference the way things used to work, in case we run into a problem or decide to convert something old.

**Coordinator Pre-2016:**

![GSIHealth - Coordinator Pre-2016](/images/architecture/legacy.png)

### Other GSIHealth Tools & Applications
We also have a few tools that we've created to work with Coordinator, that aren't actually a part of the application. In addition, we've created some internal tools that GSIHealth employees use to manage and maintain Coordinator.
