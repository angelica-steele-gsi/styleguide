title: Shortcodes
---
This section houses shortcodes for custom GSI-made features that can be re-used throughout the application.

A "shortcode" is just a catch-all term for reusable components. Different frameworks & languages have different names for it. Since we use multiple, though, "shortcode" is a good umbrella term to refer to them all. 

The benefit of using shortcodes is huge! Not only does it take less time to implement, but it ensures a uniform experience for our users. 

Think about a component you use every day; Google Search for example. You use it in a browser, on your phone, and in the Google App for Android or IOS. Even though these searches are located in different places, the experience is uniform. It makes you comfortable, because you don't nee to re-learn how to use the search in different places.

Maintaining uniformity creates an experience for the user that is not only intuitive and easy to learn and remember, but feels comfortable and seamless.

Interested in making your own shortcode? [Learn more here](creating-a-shortcode).