title: Creating a Shortcode
---
Shortcodes can be created in different ways (and are called different things) depending on which UI Framework you're using. For example; in Angular, you create "Directives". In Vue, you create "Templates". 

The general idea, though, stays the same. You create a UI component, wrap it in something, and mark that wrapper with some kind of identifier. From then on, you can reference that component using a "shortcode", being the identifier you placed on the wrapper.

Learn how to create your own shortcodes from the resources below:

* [Creating a Directive in Angular](https://docs.angularjs.org/guide/directive)

* [Creating a Template in Vue](https://vuejs.org/v2/guide/components.html)