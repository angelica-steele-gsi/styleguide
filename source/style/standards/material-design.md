title: Material Design
---
Material Design is a design language written by Google in 2014. Since then, it has taken the software-design world by storm. If you've ever used a modern mobile or browser application, chances are, you've interacted with software that follows Material Design. The entire software industry has moved in the direction of Material, and it has become somewhat of a standard. Everywhere you look, from Facebook, to Twitter, Pinterest, Instagram, and even Microsoft Products used in the browser, these products either use Google's exact Material Design standards, or take **extremely** heavy influence from it. 

Quoting from Google:

*"Material Design is a visual language that synthesizes the classic principles of good design with the innovation of technology and science."*

The design language is simple, comfortable, familiar, and easy to use, providing an enjoyable experience for users straight out of the box. When following these guidelines, being "pretty" and having a nice "look-and-feel" are already taken care of. 

All of GSI's newest features are written in Angular or Quasar, both following Material Design (Angular is a UI Framework written by Google, the creators of Material, and Vue is an independent framework written to expand on the good that Angular did for the industry and move it forward). Reading the following documentation is one of the **most crucial** steps to becoming a good designer at GSIHealth.

[Read the official Google Material Design documentation here](https://material.io/design/introduction/).