title: The 7 Principles of Design
---
UI Design is, just as it is named - User Interface Design. And design; a type of art. Just like all other types of art, knowledge of some basic principles will help ground you when creating new works. 

Check out the PDF below to learn about the **7 Principles of Good Design** and improve your designing abilities.

<object data="/documents/Articulate-Essential-Guide-to-Visual-Design.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="/documents/Articulate-Essential-Guide-to-Visual-Design.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="/documents/Articulate-Essential-Guide-to-Visual-Design.pdf">Download PDF</a>.</p>
    </embed>
</object>