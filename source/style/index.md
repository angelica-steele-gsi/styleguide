title: Styleguide
---

![Quasar - Empowering your websites and apps](/images/responsive-logo.png)

## Style and Experience - Why is it important?
Looking pretty and "feeling nice" may not seem like a priority when writing medical software, but that's a bias that has held the medical industry back for decades, and causes thousands of healthcare professionals daily frustration.

No matter how well an application does its job, if it feels uncomfortable or is frustrating to use, users won't like it. This gives competitors who may not have as much functionality, but "feel" better to interact with, the upperhand. 

Think of a time you've weighed your options between 2 choices. Let's say you're going to school, and have the choice between 2 textbooks for your class.

* One is big, worn, and **full** of information covering a **huge** range of topics in the subject you're studying. But because it's old, pages have fallen out, and they're scattered in the wrong places throughout the book. The text is written in old-English, so it's hard for you to wrap your head around some of the words.

* The other book is thin, with nowhere near as much content as the other book. But, the chapters and sections are clearly labeled, the content is written in an easy-to-understand format, by someone who isn't trying to impress you by using big words but instead focuses on getting an idea accross to as wide an audience as possible. And bonus points! Some nice student before you marked up the book with sticky notes pointing out all of the major points you'll need to memorize for your next exam.

Which book would you choose? Obviously, the older, bigger book has a lot more to offer, but for a busy student with 6 other classes to go to and a part-time job on the side, that crash-course book is looking a lot more attractive.

The reason for this is that the second book provides a better *experience*. The author who wrote the book had **you** and your life in mind while writing it. They took the time to put themselves in your shoes and respect your time and needs. The book is written with intention, and caters directly to its audience.

*This* is why style, *experience* is important. Because if you make your user enjoy using your application, they're a lot more likely to use it than if it were a dusty old behemoth that's intimidating to even start using.

## Consistency Matters
(Todo)
