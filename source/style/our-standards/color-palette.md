title: Color Palette
---
GSIHealth Coordinator uses the standard Material Design color palette. The entire palette with HEX codes [can be found here](https://www.materialui.co/colors). Click on any color to copy its HEX code to your clipboard.

To learn about the Material Design standards for color usage, [check out the guide here](https://material.io/design/color/the-color-system.html).