title: Iconography
---
GSIHealth Coordinator uses the standard Material Design icons. A list of the icons with their names [can be found here](https://material.io/tools/icons/?style=baseline).

All icons in this list are already implemented into Coordinator and can be used at any time.

To learn about the Material Design standards for icon usage, [check out the guide here](https://material.io/design/iconography/system-icons.html).

### Custom Icons

A good secondary resource for even more options, created & maintained by the public [can be found here](https://materialdesignicons.com/). This set of icons is ever-updating with more and more custom options, designed to look exactly like the Material Design icon set, and conform to Material Design standards.

Many of the icons in this list have been imported into Coordinator already, but the whole set has not. However, it is very easy to import a needed icon.

[Learn more about icon functionality and importing here]().